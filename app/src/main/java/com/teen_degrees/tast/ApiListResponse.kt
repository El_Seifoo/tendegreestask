package com.teen_degrees.tast

import com.google.gson.annotations.SerializedName

class ApiListResponse<T> {
    @SerializedName("total_pages")
    var totalPages: Int = 0
    @SerializedName(value = "results", alternate = ["profiles"])
    lateinit var list: List<T>

}