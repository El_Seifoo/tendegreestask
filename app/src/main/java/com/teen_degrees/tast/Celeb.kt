package com.teen_degrees.tast

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.gson.annotations.SerializedName
import com.teen_degrees.tast.utils.URLs
import java.io.Serializable



open class Celeb(
    @SerializedName("name") val name: String,
    @SerializedName("profile_path") val photo: String?,
    @SerializedName("id") val id: String,
    @SerializedName("known_for_department") val department: String?
)