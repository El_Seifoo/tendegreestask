package com.teen_degrees.tast

import com.google.gson.annotations.SerializedName

class CelebDetails(
    name: String,
    photo: String?,
    id: String,
    department: String?,
    @SerializedName("birthday") val birthDay: String?,
    @SerializedName("deathday") val deathDay: String?,
    @SerializedName("biography") val biography: String
) : Celeb(name, photo, id, department) {
}