package com.teen_degrees.tast

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Image(@SerializedName("file_path") val photo: String) : Serializable