package com.teen_degrees.tast.celeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.teen_degrees.tast.Celeb
import com.teen_degrees.tast.R
import com.teen_degrees.tast.celeb_details.CelebDetailsActivity
import com.teen_degrees.tast.databinding.ActivityMainBinding
import com.teen_degrees.tast.search.SearchActivity

class CelebsActivity : AppCompatActivity(), CelebsViewModel.ViewListener,
    CelebsAdapter.OnItemClicked {


    lateinit var viewModel: CelebsViewModel
    lateinit var dataBinding: ActivityMainBinding
    lateinit var list: RecyclerView
    lateinit var adapter: CelebsAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(CelebsViewModel::class.java)
        viewModel.viewListener = this
        dataBinding.viewModel = viewModel

        supportActionBar!!.setTitle(getString(R.string.popular_celebs))

        list = dataBinding.celebsList
        list.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        adapter = CelebsAdapter(this)
        list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                viewModel.onScrolled(recyclerView, dx, dy)
            }
        })

        viewModel.requestPopular().observe(this, object : Observer<List<Celeb>> {
            override fun onChanged(celebList: List<Celeb>?) {

                if (adapter.itemCount == 0)
                    adapter.addList(celebList as MutableList<Celeb>)
                else {
                    list.scrollToPosition((list.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition());
                    adapter.updateList(celebList as MutableList<Celeb>)
                }

                list.adapter = adapter
            }

        })
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onClick(id: String, name: String) {
        var intent = Intent(this, CelebDetailsActivity::class.java)
        intent.putExtra("id", id)
        intent.putExtra("name", name)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.celebs_activity_action_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_search) {
            startActivity(Intent(this, SearchActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
