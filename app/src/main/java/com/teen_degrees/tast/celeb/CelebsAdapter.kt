package com.teen_degrees.tast.celeb

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.teen_degrees.tast.Celeb
import com.teen_degrees.tast.databinding.CelebListItemBinding

class CelebsAdapter(val clickListener: OnItemClicked) :
    RecyclerView.Adapter<CelebsAdapter.Holder>() {
    var list: MutableList<Celeb> = mutableListOf()


    fun addList(list: MutableList<Celeb>) {
        this.list = list
        notifyDataSetChanged()
    }

    fun updateList(list: MutableList<Celeb>) {
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    interface OnItemClicked {
        fun onClick(id: String, name: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder.from(parent)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(clickListener, list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }

    class Holder private constructor(val dataBinding: CelebListItemBinding) :
        RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(clickListener: OnItemClicked, celeb: Celeb) {
            dataBinding.celeb = celeb
            dataBinding.onClick = clickListener
            dataBinding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): Holder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CelebListItemBinding.inflate(layoutInflater, parent, false)
                return Holder(binding)
            }
        }
    }
}