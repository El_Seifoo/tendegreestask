package com.teen_degrees.tast.celeb

import android.app.Application
import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.teen_degrees.tast.Celeb
import com.teen_degrees.tast.R
import java.io.IOException

class CelebsViewModel(application: Application) : AndroidViewModel(application),
    CelebsModel.ModelCallback {

    private var model: CelebsModel
    lateinit var viewListener: ViewListener
    var progress: ObservableField<Int>
    var errorView: ObservableField<Int>
    var emptyListTextView: ObservableField<Int>
    var errorMessage: ObservableField<String>
    val isLoading: ObservableField<Boolean> = ObservableField(false)
    var page: Int = 1
    var totalPages: Int = 2

    init {
        model = CelebsModel()
        progress = ObservableField(View.GONE)
        errorView = ObservableField(View.GONE)
        emptyListTextView = ObservableField(View.GONE)
        errorMessage = ObservableField("")
    }

    fun requestPopular(): MutableLiveData<List<Celeb>> {
        if (page > totalPages) {
            var mutableLiveData: MutableLiveData<List<Celeb>> = MutableLiveData()
            mutableLiveData.value = ArrayList<Celeb>()
            return mutableLiveData
        }

        isLoading.set(true)
        if (page == 1)
            progress.set(View.VISIBLE);
        return model.fetchPopular(
            page,
            getApplication<Application>().getString(R.string.api_key),
            this
        )
    }

    fun onRetryClickListener(view: View) {
        setProgress(View.VISIBLE)
        errorView.set(View.GONE);
        model.fetchPopular(
            page,
            getApplication<Application>().getString(R.string.api_key),
            this
        )
    }


    override fun setProgress(progress: Int) {
        this.progress.set(progress)
    }

    override fun increasePage() {
        page++
    }

    override fun updateTotalPages(totalPages: Int) {
        this.totalPages = totalPages
    }

    override fun updateEmptyListTextView(empty: Int) {
        emptyListTextView.set(empty)
    }

    override fun handleRequestResponse(responseCode: Int, page: Int) {
        if (page > 1) {
            viewListener.showToastMessage(returnErrorMessage(responseCode))
        } else {
            errorView.set(View.VISIBLE);
            errorMessage.set(returnErrorMessage(responseCode))
        }
    }

    private fun returnErrorMessage(responseCode: Int): String {
        if (responseCode == 401) return getApplication<Application>().getString(R.string.invalid_api_key)
        return getApplication<Application>().getString(R.string.api_not_found)
    }

    override fun onFailureHandler(t: Throwable, page: Int) {
        if (page > 1) {
            viewListener.showToastMessage(returnErrorMessage(t))
        } else {
            errorView.set(View.VISIBLE)
            errorMessage.set(returnErrorMessage(t))
        }
    }

    private fun returnErrorMessage(t: Throwable): String {
        if (t is IOException) return (getApplication<Application>().getString(R.string.no_internet_connection))
        return getApplication<Application>().getString(R.string.error_fetching_data)
    }

    fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (dy > 0) {
            val pastVisibleItem =
                (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
            val total = recyclerView.adapter!!.itemCount
            Log.e("isloading", isLoading.get().toString())
            if (!isLoading.get()!!) {
                if (pastVisibleItem >= total - 1) {
                    this.requestPopular()
                }

            }
        }
    }


    override fun setIsLoading(boolean: Boolean) {
        isLoading.set(boolean)
    }

    interface ViewListener {
        fun showToastMessage(message: String)
    }
}