package com.teen_degrees.tast.celeb_details

import android.content.Intent
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.teen_degrees.tast.CelebDetails
import com.teen_degrees.tast.Image
import com.teen_degrees.tast.R
import com.teen_degrees.tast.databinding.ActivityCelebDetailsBinding
import com.teen_degrees.tast.photo.PhotoActivity

class CelebDetailsActivity : AppCompatActivity(), CelebImagesAdapter.OnItemClicked {
    lateinit var viewModel: CelebsDetailsViewModel
    lateinit var dataBinding: ActivityCelebDetailsBinding
    lateinit var list: RecyclerView
    lateinit var adapter: CelebImagesAdapter
    lateinit var name: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_celeb_details)
        viewModel = ViewModelProviders.of(this).get(CelebsDetailsViewModel::class.java)
        dataBinding.viewModel = viewModel

        supportActionBar!!.setTitle("")
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        name = intent.getStringExtra("name");

        viewModel.requestCelebDetails(intent.extras!!.getString("id", "")).observe(this,
            object : Observer<CelebDetails> {
                override fun onChanged(celeb: CelebDetails?) {
                    dataBinding.celeb = celeb
                }

            })

        list = dataBinding.imagesList
        list.layoutManager = GridLayoutManager(this, viewModel.calculateSpanCount())
        list.addItemDecoration(
            GridSpacingItemDecoration(
                viewModel.calculateSpanCount(),
                viewModel.dpToPx(2),
                true
            )
        )
        list.itemAnimator = DefaultItemAnimator()
//        list.isNestedScrollingEnabled = false
        adapter = CelebImagesAdapter(this)

        viewModel.requestCelebImages(intent.extras!!.getString("id", "")).observe(this,
            object : Observer<List<Image>> {
                override fun onChanged(images: List<Image>?) {
                    adapter.addList(images as MutableList<Image>)
                    list.adapter = adapter
                }

            })
    }


    override fun onClick(url: String) {
        var intent = Intent(this, PhotoActivity::class.java)
        intent.putExtra("name", name)
        intent.putExtra("photo", url)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
