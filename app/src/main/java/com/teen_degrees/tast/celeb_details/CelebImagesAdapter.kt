package com.teen_degrees.tast.celeb_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.teen_degrees.tast.Image
import com.teen_degrees.tast.databinding.ImagesListItemBinding

class CelebImagesAdapter(val clickListener: OnItemClicked) :
    RecyclerView.Adapter<CelebImagesAdapter.Holder>() {
    var list: MutableList<Image> = mutableListOf()

    fun addList(list: MutableList<Image>) {
        this.list = list
        notifyDataSetChanged()
    }


    interface OnItemClicked {
        fun onClick(url: String)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CelebImagesAdapter.Holder {
        return Holder.from(parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(clickListener, list.get(position))
    }


    class Holder private constructor(val dataBinding: ImagesListItemBinding) :
        RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(clickListener: OnItemClicked, image: Image) {
            dataBinding.image = image
            dataBinding.onClick = clickListener
            dataBinding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): Holder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ImagesListItemBinding.inflate(layoutInflater, parent, false)
                return Holder(binding)
            }
        }
    }
}
