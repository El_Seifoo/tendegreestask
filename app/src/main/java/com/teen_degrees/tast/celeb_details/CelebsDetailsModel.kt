package com.teen_degrees.tast.celeb_details

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.teen_degrees.tast.ApiListResponse
import com.teen_degrees.tast.CelebDetails
import com.teen_degrees.tast.Image
import com.teen_degrees.tast.utils.MySingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CelebsDetailsModel {
    private var celebMutableLiveData = MutableLiveData<CelebDetails>()

    fun fetchCelebDetails(
        id: String, apiKey: String, callback: ModelCallback
    ): MutableLiveData<CelebDetails> {
        if (celebMutableLiveData.value == null) {
            var call: Call<CelebDetails> =
                MySingleton.getInstance().createService().fetchCelebDetails(id, apiKey)

            call.enqueue(object : Callback<CelebDetails> {
                override fun onResponse(
                    call: Call<CelebDetails>,
                    response: Response<CelebDetails>
                ) {
                    callback.setProgress(View.GONE)
                    if (response.code() == 401 || response.code() == 404) {
                        callback.handleRequestResponse(401, false)
                        return
                    }

                    if (response.code() == 200) {
                        celebMutableLiveData.value = response.body()
                    }
                }

                override fun onFailure(call: Call<CelebDetails>, t: Throwable) {
                    callback.setProgress(View.GONE)
                    callback.onFailureHandler(t, false)
                }

            })
        } else {
            callback.setProgress(View.GONE)
        }

        return celebMutableLiveData
    }

    private var celebImagesMutableLiveData = MutableLiveData<List<Image>>()
    fun fetchCelebImages(
        id: String, apiKey: String, callback: ModelCallback
    ): MutableLiveData<List<Image>> {
        if (celebImagesMutableLiveData.value == null) {
            var call: Call<ApiListResponse<Image>> =
                MySingleton.getInstance().createService().fetchCelebImages(id, apiKey)

            call.enqueue(object : Callback<ApiListResponse<Image>> {

                override fun onResponse(
                    call: Call<ApiListResponse<Image>>,
                    response: Response<ApiListResponse<Image>>
                ) {
                    callback.setListProgress(View.GONE)
                    if (response.code() == 401 || response.code() == 404) {
                        callback.handleRequestResponse(401, true)
                        return
                    }

                    if (response.code() == 200) {
                        if (response.body()?.list == null) {
                            callback.updateEmptyListTextView(View.VISIBLE)
                            celebImagesMutableLiveData.value = ArrayList<Image>()
                            return
                        }
                        if (response.body()?.list!!.isEmpty()) {
                            callback.updateEmptyListTextView(View.VISIBLE)
                        }
                        celebImagesMutableLiveData.value = response.body()?.list
                    }
                }

                override fun onFailure(call: Call<ApiListResponse<Image>>, t: Throwable) {
                    callback.setListProgress(View.GONE)
                    callback.onFailureHandler(t, true)
                }

            })
        } else {
            callback.setListProgress(View.GONE)
        }

        return celebImagesMutableLiveData
    }


    interface ModelCallback {
        fun setProgress(progress: Int)
        fun setListProgress(progress: Int)
        fun updateEmptyListTextView(empty: Int)
        fun handleRequestResponse(responseCode: Int, fetchingList: Boolean)
        fun onFailureHandler(t: Throwable, fetchingList: Boolean)
    }

}