package com.teen_degrees.tast.celeb_details

import android.app.Application
import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.teen_degrees.tast.CelebDetails
import com.teen_degrees.tast.Image
import com.teen_degrees.tast.R
import java.io.IOException

class CelebsDetailsViewModel(application: Application) : AndroidViewModel(application),
    CelebsDetailsModel.ModelCallback {


    private val model: CelebsDetailsModel = CelebsDetailsModel()
    lateinit var viewListener: ViewListener
    val progress: ObservableField<Int> = ObservableField(View.GONE)
    val listProgress: ObservableField<Int> = ObservableField(View.GONE)
    var emptyListTextView: ObservableField<Int> = ObservableField(View.GONE)
    var errorView: ObservableField<Int> = ObservableField(View.GONE)
    var listErrorView: ObservableField<Int> = ObservableField(View.GONE)
    var errorMessage: ObservableField<String> = ObservableField("")
    var listErrorMessage: ObservableField<String> = ObservableField("")

    lateinit var id: String
    fun requestCelebDetails(id: String): MutableLiveData<CelebDetails> {
        this.id = id
        progress.set(View.VISIBLE)
        return model.fetchCelebDetails(
            id,
            getApplication<Application>().getString(R.string.api_key),
            this
        )
    }

    fun requestCelebImages(id: String): MutableLiveData<List<Image>> {
        listProgress.set(View.VISIBLE)
        return model.fetchCelebImages(
            id,
            getApplication<Application>().getString(R.string.api_key),
            this
        )
    }


    fun onRetryClickListener(view: View, fetchingList: Boolean) {
        if (fetchingList) {
            setListProgress(View.VISIBLE)
            listErrorView.set(View.GONE)
            model.fetchCelebImages(
                id,
                getApplication<Application>().getString(R.string.api_key),
                this
            )
        } else {
            setProgress(View.VISIBLE)
            errorView.set(View.GONE);
            model.fetchCelebDetails(
                id,
                getApplication<Application>().getString(R.string.api_key),
                this
            )
        }

    }


    override fun setProgress(progress: Int) {
        this.progress.set(progress)
    }

    override fun setListProgress(progress: Int) {
        this.listProgress.set(progress)
    }

    override fun updateEmptyListTextView(empty: Int) {
        emptyListTextView.set(empty)
    }
    override fun handleRequestResponse(responseCode: Int, fetchingList: Boolean) {
        if (fetchingList) {
            listErrorView.set(View.VISIBLE)
            listErrorMessage.set(returnErrorMessage(responseCode))
        } else {
            errorView.set(View.VISIBLE);
            errorMessage.set(returnErrorMessage(responseCode))
        }
    }

    private fun returnErrorMessage(responseCode: Int): String {
        if (responseCode == 401) return getApplication<Application>().getString(R.string.invalid_api_key)
        return getApplication<Application>().getString(R.string.api_not_found)
    }

    override fun onFailureHandler(t: Throwable, fetchingList: Boolean) {
        if (fetchingList) {
            listErrorView.set(View.VISIBLE)
            listErrorMessage.set(returnErrorMessage(t))
        } else {
            errorView.set(View.VISIBLE)
            errorMessage.set(returnErrorMessage(t))
        }
    }

    private fun returnErrorMessage(t: Throwable): String {
        if (t is IOException) return (getApplication<Application>().getString(R.string.no_internet_connection))
        return getApplication<Application>().getString(R.string.error_fetching_data)
    }


    fun dpToPx(dp: Int): Int {
        var resources: Resources = getApplication<Application>().getResources()
        return Math.round(
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                resources.getDisplayMetrics()
            )
        )
    }

    fun calculateSpanCount(): Int {
        var displayMetrics: DisplayMetrics = getApplication<Application>().resources.displayMetrics
        var dpWidth: Float = displayMetrics.widthPixels / displayMetrics.density
        return (dpWidth / 80).toInt()
    }


    interface ViewListener {
        fun showToastMessage(message: String)
    }
}