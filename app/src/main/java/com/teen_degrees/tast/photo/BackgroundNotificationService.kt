package com.teen_degrees.tast.photo

import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Environment
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.teen_degrees.tast.utils.MySingleton
import okhttp3.ResponseBody
import retrofit2.Call
import java.io.*

class BackgroundNotificationService(val name: String = "Service") : IntentService(name) {
    private lateinit var notificationBuilder: NotificationCompat.Builder
    private lateinit var notificationManager: NotificationManager

    override fun onHandleIntent(intent: Intent?) {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var notificationChannel =
                NotificationChannel("id", "Channel", NotificationManager.IMPORTANCE_LOW)

            notificationChannel.description = "no sound"
            notificationChannel.setSound(null, null)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.BLUE
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        notificationBuilder = NotificationCompat.Builder(this, "id")
            .setSmallIcon(android.R.drawable.stat_sys_download)
            .setContentTitle("download")
            .setContentText("Downloading Image")
            .setDefaults(0)
            .setAutoCancel(true)
        notificationManager.notify(0, notificationBuilder.build())
        Log.e(
            "url",
            intent!!.extras!!.getString("image") + ",," + intent.extras!!.getString("name")
        )
        initRetrofit(intent!!.extras!!.getString("image"), intent.extras!!.getString("name"))
    }

    private fun initRetrofit(url: String?, name: String?) {
        var call: Call<ResponseBody> =
            MySingleton.getInstance().createService().downloadImage(url!!)

        downloadImage(call.execute().body(), name)
    }

    private fun downloadImage(body: ResponseBody?, name: String?) {


        var count = 0
        var data = ByteArray(4096)
        var fileSize: Long = body!!.contentLength()
        var inputStream: InputStream = body.byteStream()
        var file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            name.plus(".jpg")
        )
        var outPutStream = FileOutputStream(file)
        var total: Long = 0
        var progress = 0
        var downloadComplete = false
        count = inputStream.read(data)
        while (count != -1) {
            total += count
            progress = ((total * 100).toDouble() / fileSize.toDouble()).toInt()
            updateNotification(progress)
            outPutStream.write(data, 0, count)
            downloadComplete = true
            count = inputStream.read(data)
        }

        onDownloadComplete(downloadComplete)
        outPutStream.flush()
        outPutStream.close()
        inputStream.close()
    }

    private fun updateNotification(currentProgress: Int) {
        notificationBuilder.setProgress(100, currentProgress, false)
        notificationBuilder.setContentText("Downloaded".plus(currentProgress).plus("%"))
        notificationManager.notify(0, notificationBuilder.build())
    }

    private fun sendProgressUpdate(downloadComplete: Boolean) {
        var intent = Intent(PhotoActivity.PROGRESS_UPDATE)
        intent.putExtra("downloadComplete", downloadComplete)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun onDownloadComplete(downloadComplete: Boolean) {
        sendProgressUpdate(downloadComplete);

        notificationManager.cancel(0)
        notificationBuilder.setProgress(0, 0, false)
        notificationBuilder.setContentText("Image Download complete")
        notificationManager.notify(0, notificationBuilder.build())
    }

    override public fun onTaskRemoved(rootIntent: Intent?) {
        notificationManager.cancel(0)
    }
}