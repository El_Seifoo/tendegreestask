package com.teen_degrees.tast.photo

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.teen_degrees.tast.R
import com.teen_degrees.tast.databinding.ActivityPhotoBinding
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.teen_degrees.tast.utils.URLs


class PhotoActivity : AppCompatActivity() {
    lateinit var dataBinding: ActivityPhotoBinding
    lateinit var photo: String
    lateinit var name: String

    companion object {
        val PROGRESS_UPDATE: String = "progress_update"
    }

    val PERMISSION_REQUEST_CODE: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_photo)
        dataBinding.photo = intent.getStringExtra("photo")

        supportActionBar!!.setTitle("")
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        photo = URLs.PHOTOS_BASE + "original" + intent.getStringExtra("photo")
        name = intent.getStringExtra("name")

        registerReceiver();
    }

    private fun registerReceiver() {
        var localBroadcastManager = LocalBroadcastManager.getInstance(this)
        var intentFilter = IntentFilter()
        intentFilter.addAction(PROGRESS_UPDATE)
        localBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter)
    }

    val mBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent!!.action.equals(PROGRESS_UPDATE)) {
                var downloadComplete: Boolean = intent.getBooleanExtra("downloadComplete", false)
                if (downloadComplete) {
                    Toast.makeText(applicationContext, "Image downloaded", Toast.LENGTH_LONG).show()
                }
            }
        }

    }

    private fun checkPermission(): Boolean {
        var result = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun startDownload() {
        var intent = Intent(this, BackgroundNotificationService::class.java)
        intent.putExtra("image", photo)
        intent.putExtra("name", name)
        startService(intent)
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        when (requestCode) {
            PERMISSION_REQUEST_CODE ->
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    startDownload()
                else Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.photo_activity_action_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        if (item.itemId == R.id.action_download) {
            if (checkPermission())
                startDownload()
            else
                requestPermission()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
