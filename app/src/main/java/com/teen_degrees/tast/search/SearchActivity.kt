package com.teen_degrees.tast.search

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.teen_degrees.tast.Celeb
import com.teen_degrees.tast.R
import com.teen_degrees.tast.celeb.CelebsAdapter
import com.teen_degrees.tast.celeb_details.CelebDetailsActivity
import com.teen_degrees.tast.databinding.ActivitySearchBinding

class SearchActivity : AppCompatActivity(), SearchViewModel.ViewListener,
    CelebsAdapter.OnItemClicked {


    lateinit var viewModel: SearchViewModel
    lateinit var dataBinding: ActivitySearchBinding
    lateinit var list: RecyclerView
    lateinit var adapter: CelebsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        viewModel.viewListener = this
        dataBinding.viewModel = viewModel

        supportActionBar!!.setTitle("")
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        list = dataBinding.celebsList
        list.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        adapter = CelebsAdapter(this)
        list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                viewModel.onScrolled(recyclerView, dx, dy)
            }
        })

        viewModel.requestQuery("").observe(this, object : Observer<List<Celeb>> {
            override fun onChanged(celebList: List<Celeb>?) {

                if (adapter.itemCount == 0)
                    adapter.addList(celebList as MutableList<Celeb>)
                else {
                    list.scrollToPosition((list.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition());
                    adapter.updateList(celebList as MutableList<Celeb>)

                }

                list.adapter = adapter
            }

        })
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onClick(id: String, name: String) {
        var intent = Intent(this, CelebDetailsActivity::class.java)
        intent.putExtra("id", id)
        intent.putExtra("name", name)
        startActivity(intent)
    }

    override fun resetAdapter() {
        adapter.clear()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_activity_action_menu, menu)

        var menuItem = menu!!.findItem(R.id.action_search)
        var searchView: SearchView = menuItem.actionView as SearchView
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
//                    if (query.isEmpty()) adapter.clear()
                    viewModel.requestQuery(query)
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
//                    if (newText.isEmpty()) adapter.clear()
                    viewModel.requestQuery(newText)
                }
                return true
            }

        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
