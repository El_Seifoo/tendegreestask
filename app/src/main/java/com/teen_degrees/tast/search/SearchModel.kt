package com.teen_degrees.tast.search

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.teen_degrees.tast.ApiListResponse
import com.teen_degrees.tast.Celeb
import com.teen_degrees.tast.utils.MySingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchModel {
    private var celebsMutableLiveData = MutableLiveData<List<Celeb>>(ArrayList<Celeb>())
    fun fetchQuery(
        page: Int,
        query: String,
        apiKey: String,
        callback: ModelCallback
    ): MutableLiveData<List<Celeb>> {
//        if (query.isEmpty()){
//            callback.setIsLoading(false)
//            callback.setProgress(View.GONE)
//            return celebsMutableLiveData
//        }
        if (celebsMutableLiveData.value.isNullOrEmpty() || page > 1) {
            var call = MySingleton.getInstance().createService().search(page, query, false, apiKey);

            call.enqueue(object : Callback<ApiListResponse<Celeb>> {
                override fun onResponse(
                    call: Call<ApiListResponse<Celeb>>,
                    listResponse: Response<ApiListResponse<Celeb>>
                ) {
                    callback.setProgress(View.GONE)
                    callback.setIsLoading(false)
                    callback.increasePage()
                    if (listResponse.code() == 401 || listResponse.code() == 404) {
                        callback.handleRequestResponse(401, page)
                        return
                    }


                    if (listResponse.code() == 200) {
                        var responseObj: ApiListResponse<Celeb> = listResponse.body()!!
                        callback.updateTotalPages(responseObj.totalPages)
                        if (responseObj.list.isEmpty()) {
                            callback.updateEmptyListTextView(View.VISIBLE)
                        } else {
                            callback.updateEmptyListTextView(View.GONE)
                        }

                        celebsMutableLiveData.value = responseObj.list
                    }
                }

                override fun onFailure(call: Call<ApiListResponse<Celeb>>, t: Throwable) {
                    callback.setProgress(View.GONE)
                    callback.setIsLoading(false)
                    callback.onFailureHandler(t, page)
                }

            })


        } else {
            callback.setIsLoading(false)
            callback.setProgress(View.GONE)
        }


        return celebsMutableLiveData
    }

    fun resetList() {
        celebsMutableLiveData.value = ArrayList<Celeb>()
    }

    interface ModelCallback {
        fun setProgress(progress: Int)
        fun setIsLoading(boolean: Boolean)
        fun increasePage()
        fun updateTotalPages(totalPages: Int)
        fun updateEmptyListTextView(visible: Int)
        fun handleRequestResponse(responseCode: Int, page: Int)
        fun onFailureHandler(t: Throwable, page: Int)
    }
}