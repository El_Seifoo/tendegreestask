package com.teen_degrees.tast.search

import android.app.Application
import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.teen_degrees.tast.Celeb
import com.teen_degrees.tast.R
import java.io.IOException

class SearchViewModel(application: Application) : AndroidViewModel(application),
    SearchModel.ModelCallback {
    private var model = SearchModel()
    lateinit var viewListener: ViewListener
    var progress: ObservableField<Int> = ObservableField(View.GONE)
    var errorView: ObservableField<Int> = ObservableField(View.GONE)
    var emptyListTextView: ObservableField<Int> = ObservableField(View.VISIBLE)
    var errorMessage: ObservableField<String> = ObservableField("")
    val isLoading: ObservableField<Boolean> = ObservableField(false)
    var page: Int = 1
    var totalPages: Int = 2
    var query: String = ""

    fun requestQuery(query: String): MutableLiveData<List<Celeb>> {
//        if (query.isEmpty() || page > totalPages) {
//            if (query.isEmpty())
//                emptyListTextView.set(View.VISIBLE)
//
//            var mutableLiveData: MutableLiveData<List<Celeb>> = MutableLiveData()
//            mutableLiveData.value = ArrayList<Celeb>()
//            return mutableLiveData
//        }

        if (!query.equals(this.query)) {
            page = 1
            viewListener.resetAdapter()
            model.resetList()
            emptyListTextView.set(View.VISIBLE)
        }

        this.query = query
        isLoading.set(true)
        if (page == 1)
            progress.set(View.VISIBLE);
        return model.fetchQuery(
            page,
            query,
            getApplication<Application>().getString(R.string.api_key),
            this
        )
    }

    fun onRetryClickListener(view: View) {
        setProgress(View.VISIBLE)
        errorView.set(View.GONE);
        model.fetchQuery(
            page,
            query,
            getApplication<Application>().getString(R.string.api_key),
            this
        )
    }

    override fun setProgress(progress: Int) {
        this.progress.set(progress)
    }

    override fun increasePage() {
        page++
    }

    override fun updateTotalPages(totalPages: Int) {
        this.totalPages = totalPages
    }

    override fun updateEmptyListTextView(empty: Int) {
        emptyListTextView.set(empty)
    }

    override fun handleRequestResponse(responseCode: Int, page: Int) {
        if (page > 1) {
            viewListener.showToastMessage(returnErrorMessage(responseCode))
        } else {
            errorView.set(View.VISIBLE);
            errorMessage.set(returnErrorMessage(responseCode))
        }
    }

    private fun returnErrorMessage(responseCode: Int): String {
        if (responseCode == 401) return getApplication<Application>().getString(R.string.invalid_api_key)
        return getApplication<Application>().getString(R.string.api_not_found)
    }

    override fun onFailureHandler(t: Throwable, page: Int) {
        if (page > 1) {
            viewListener.showToastMessage(returnErrorMessage(t))
        } else {
            errorView.set(View.VISIBLE)
            errorMessage.set(returnErrorMessage(t))
        }
    }

    private fun returnErrorMessage(t: Throwable): String {
        if (t is IOException) return (getApplication<Application>().getString(R.string.no_internet_connection))
        return getApplication<Application>().getString(R.string.error_fetching_data)
    }

    fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (dy > 0) {
            val pastVisibleItem =
                (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
            val total = recyclerView.adapter!!.itemCount
            Log.e("isloading", isLoading.get().toString())
            if (!isLoading.get()!!) {
                if (pastVisibleItem >= total - 1) {
                    this.requestQuery(query)
                }

            }
        }
    }

    override fun setIsLoading(boolean: Boolean) {
        isLoading.set(boolean)
    }

    interface ViewListener {
        fun showToastMessage(message: String)
        fun resetAdapter()
    }
}