package com.teen_degrees.tast.utils

import com.teen_degrees.tast.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiServicesClient {
    @GET(URLs.CELEBS)
    fun fetchPopular(
        @Query("page") page: Int,
        @Query("api_key") apiKey: String
    ): Call<ApiListResponse<Celeb>>;

    @GET(URLs.CELEB_DETAILS + "{person_id}")
    fun fetchCelebDetails(
        @Path("person_id") id: String,
        @Query("api_key") apiKey: String
    ): Call<CelebDetails>;

    @GET(URLs.CELEB_DETAILS + "{person_id}/images")
    fun fetchCelebImages(
        @Path("person_id") id: String,
        @Query("api_key") apiKey: String
    ): Call<ApiListResponse<Image>>;

    @GET()
    @Streaming
    fun downloadImage(@Url imageUrl: String): Call<ResponseBody>

    @GET(URLs.SEARCH)
    fun search(
        @Query("page") page: Int,
        @Query("query") query: String,
        @Query("include_adult") includeAdult: Boolean,
        @Query("api_key") apiKey: String
    ): Call<ApiListResponse<Celeb>>;


}