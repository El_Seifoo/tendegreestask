package com.teen_degrees.tast.utils

import android.content.Context
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.teen_degrees.tast.R

class BindingUtils {
    companion object {
        @JvmStatic
        fun setDepartment(department: String?): String {
            if (department == null) return ""
            return "(".plus(department).plus(")")
        }

        @JvmStatic
        fun handleBirthDeathDay(context: Context, birthDay: String?, deathDay: String?): String {
            if (birthDay == null && deathDay == null) return ""
            if (birthDay != null && deathDay == null) return context.getString(R.string.born)
                .plus(":").plus(birthDay)
            if (birthDay != null && deathDay != null) return context.getString(R.string.born)
                .plus(":").plus(birthDay).plus("\n").plus(context.getString(R.string.died))
                .plus(":").plus(deathDay)
            return context.getString(R.string.died).plus(":").plus(deathDay)
        }

        @JvmStatic
        @BindingAdapter("loadImage")
        fun ImageView.setLoadImage(photo: String?) {
            Glide.with(this.context)
                .load(URLs.PHOTOS_BASE + "w185" + photo)
                .error(R.mipmap.ic_launcher)
                .into(this)
        }

        @JvmStatic
        @BindingAdapter("loadOriginalImage")
        fun ImageView.setLoadOriginalImage(photo: String?) {
            Glide.with(this.context)
                .load(URLs.PHOTOS_BASE + "original" + photo)
                .error(R.mipmap.ic_launcher)
                .into(this)
        }


    }
}