package com.teen_degrees.tast.utils

class URLs {
    companion object {
        const val PHOTOS_BASE = "http://image.tmdb.org/t/p/"
        const val ROOT = "http://api.themoviedb.org/3/"
        const val CELEBS = "person/popular"
        const val CELEB_DETAILS = "person/"
        const val CELEB_IMAGES = "person/"
        const val SEARCH = "search/person"
    }
}